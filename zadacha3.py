import random
array = [0] * 20
sum = 0

print("|", end=" ")
for i in range(len(array)):
    a = random.randint(-10, 10)
    array[i] = a
    print(array[i], end=" ")
print("|", end=" ")

print()
print("отсортированный массив")

print("|", end=" ")
for i in range(len(array)):
    array.sort()
    print(array[i], end=" ")
print("|", end=" ")

print()
value = int(input())

mid = len(array) // 2
low = 0
high = len(array) - 1
 
while array[mid] != value and low <= high:
    if value > array[mid]:
        low = mid + 1
    else:
        high = mid - 1
    mid = (low + high) // 2
 
if low > high:
    print("No value")
else:
    print("ID =", mid)