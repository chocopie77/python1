import random
array = [0] * 20
sum = 0
print("|", end=" ")
for i in range(len(array)):
    a = random.randint(-10, 10)
    array[i] = a
    if a >= 0:
        sum += 1
    print(array[i], end=" ")
print("|")
print()
print(" Положительных числе в массиве: " + str(sum))